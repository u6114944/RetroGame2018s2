package com.example.game;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Handler;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ItemAdapter extends BaseAdapter {

    private List<FruitData> objects = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;
    private boolean checkout = false;
    private List<EditText> mTextViews;
    private List<ImageView> mImageViews;
    private android.os.Handler mHandler = new android.os.Handler();

    public ItemAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        mTextViews = new ArrayList<>();
        mImageViews = new ArrayList<>();
    }

    public void reset() {
        for (EditText e : mTextViews) {
            if (e != null) {
                e.setText("");
            }
        }
        checkout = false;
        mTextViews.clear();
        mImageViews.clear();
        objects = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void clearInput() {
        for (EditText e : mTextViews) {
            if (e != null) {
                e.setText("");
            }
        }
        for (FruitData e : objects) {
            if (e != null) {
                e.setInput("");
            }
        }
    }

    public void setObjects(List<FruitData> objects, int seconds) {
        this.objects = objects;
        notifyDataSetChanged();
        for (ImageView m : mImageViews) {
            if (m != null) {
                m.setVisibility(View.VISIBLE);
            }
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (ImageView m : mImageViews) {
                    if (m != null) {
                        m.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }, seconds * 120L);
    }

    public void startCheckout() {
        for (FruitData e : objects) {
            if (e != null) {
                String s = e.getInput();
                if (TextUtils.isEmpty(s)) {
                    Toast.makeText(context, "The answer cannot be empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }
        checkout = true;
        notifyDataSetChanged();
        checkout = false;
        MainActivity activity = (MainActivity) context;
        for (FruitData d : objects) {
            if (!d.ok()) {
                activity.result(false);
                return;
            }
        }
        activity.result(true);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public FruitData getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(final FruitData object, ViewHolder holder) {
        holder.img.setImageResource(object.getId());
        holder.et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                object.setInput(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mTextViews.add(holder.et);
        mImageViews.add(holder.img);
        if (checkout) {
            boolean ok = object.ok();
            holder.et.setTextColor(ok ? 0xff00ff00 : 0xffff0000);
        } else {
            holder.et.setTextColor(0xff333333);
        }
    }

    protected class ViewHolder {
        private ImageView img;
        private EditText et;

        public ViewHolder(View view) {
            img = (ImageView) view.findViewById(R.id.img);
            et = (EditText) view.findViewById(R.id.et);
        }
    }
}
