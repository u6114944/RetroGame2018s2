package com.example.game;

import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final int S = 20;

    private GridView grid;
    private ItemAdapter mItemAdapter;
    private Button btn;
    private ImageView bg;
    private boolean first = true;
    private int currentGrade = 1;
    private int questionCount = 3;
    private int inscreaCount = 1;
    private int maxGrade = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = findViewById(R.id.btn);
        grid = findViewById(R.id.grid);
        bg = findViewById(R.id.bg);
        grid.setAdapter(mItemAdapter = new ItemAdapter(this));
    }

    public void start(View view) {
        CharSequence text = btn.getText();
        if ("START".equalsIgnoreCase(text.toString())) {
            startGameOrNextGrade();
        } else {
            submitYourAnswer();
        }
    }

    private void submitYourAnswer() {
        mItemAdapter.startCheckout();
    }

    private void startGameOrNextGrade() {
        bg.setVisibility(View.GONE);
        btn.setText("SUBMIT");
        if (!first) {
            currentGrade += 1;
            if (currentGrade <= maxGrade) {
                questionCount += inscreaCount;
            } else {
                Toast.makeText(this, "Congratulations on completing the game.", Toast.LENGTH_SHORT).show();
                reset();
                return;
            }
        }
        mItemAdapter.clearInput();
        mItemAdapter.setObjects(FruitData.getData(questionCount), S);
    }

    private void reset() {
        first = true;
        currentGrade = 1;
        mItemAdapter.reset();
        btn.setText("START");
        bg.setVisibility(View.VISIBLE);
    }

    public void result(boolean ok) {
        if (ok) {
            if (currentGrade >= maxGrade) {
                new AlertDialog.Builder(this)
                        .setTitle("Game Result")
                        .setMessage("Congratulations on completing the game.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                reset();
                            }
                        }).show();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Game Result")
                        .setMessage("Congratulations on passing level " + currentGrade + ". You are about to enter the next level.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startGameOrNextGrade();
                            }
                        }).show();

            }
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Game Result")
                    .setMessage("The game failed.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            reset();
                        }
                    }).show();
        }
    }
}
