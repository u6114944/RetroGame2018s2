package com.example.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FruitData {

    public static List<FruitData> sFruitData;

    static {
        sFruitData = new ArrayList<>();
        sFruitData.add(new FruitData(R.mipmap.apple, "apple"));
        sFruitData.add(new FruitData(R.mipmap.banana, "banana"));
        sFruitData.add(new FruitData(R.mipmap.grape, "grape"));
        sFruitData.add(new FruitData(R.mipmap.kiwi, "kiwi"));
        sFruitData.add(new FruitData(R.mipmap.orange, "orange"));
        sFruitData.add(new FruitData(R.mipmap.peach, "peach"));
        sFruitData.add(new FruitData(R.mipmap.pear, "pear"));
        sFruitData.add(new FruitData(R.mipmap.pineapple, "pineapple"));
        sFruitData.add(new FruitData(R.mipmap.strawberry, "strawberry"));
        sFruitData.add(new FruitData(R.mipmap.watermelon, "watermelon"));
    }

    public static List<FruitData> getData(int count) {
        Collections.shuffle(sFruitData);
        return sFruitData.subList(0, Math.min(sFruitData.size(), count));
    }

    public static boolean allCorrect(List<FruitData> data) {
        if (data == null || data.size() == 0) {
            return false;
        }
        for (FruitData d : data) {
            if (!d.ok())
                return false;
        }
        return false;
    }

    private int id;
    private String name;
    private String input;

    public FruitData() {
    }

    public FruitData(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public boolean ok(String input) {
        return name.equalsIgnoreCase(input);
    }

    public boolean ok() {
        return name.equalsIgnoreCase(input);
    }
}
